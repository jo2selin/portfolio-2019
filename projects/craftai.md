---
path: /projects/craftai
date: Aout 2019
title: Craft.ai
contentType: projet
lien: "https://craft.ai/"
tags:
  - CMS
  - gatsby
  - markdown
  - freelance
projectImage: /assets/craftai.jpg
order: 80
---

Pour ce projet freelance, j’ai eu la chance de travailler avec Craft.ai ,une entreprise française leader dans le domaine de l’intelligence artificielle

# Etude de cas :

## Leur problème :

Nous avons un site vitrine où nous présentons nos outils. Le site permet à nos clients de découvrir nos produits et tarifs, ainsi que de nous contacter.

Nous avons également une partie blog, qui nous permet de publier nos actualités et améliorer notre référencement naturel.

Le site, réalisé en jekyll (python) est difficile à maintenir (nos développeurs préférant travailler en javascript), et ne propose pas d’interface pour rajouter des articles dans le blog.

## La solution :

Réaliser un site avec un générateur de site statique en React (Gatsby).

Ecrit en Javascript, ce générateur permet de créer un site sans serveur, le tout avec des chargement de page très rapide.

Pour gérer le contenu du site et du blog, j’ai rajouté un petit CMS (NetlifyCMS). Ecrire un article de blog ou modifier le contenu d’une page devient très facile, en quelques clic, le site est mis à jour.
