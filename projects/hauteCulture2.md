---
path: /projects/hauteculture2
date: Octobre 2019
title: Hauteculture 2
contentType: projet
lien: "https://hauteculture.com/"
tags:
  - CMS
  - API
  - gatsby
  - DevOps
  - Azure
projectImage: /assets/hauteculture2.jpg
order: 70
---# Réalisation d'un blog ultra performant grace a Gatsby.js

## Back-end :

Pour réaliser le back office de ce site, j’ai décider d’utiliser le CMS Strapi, qui permet de générer une API avec des relations entre les éléments (articles, artistes, pays …) .

Strapi est l’équivalent open source d’un Contenful ou Prismic.

## Devops :

Le site étant réalisé avec Gatsby, il a besoin d’être buildé à chaque nouvel article, j’ai donc créé un pipeline qui build le site a chaque push sur notre Git et qui va ensuite publier le site sur l’hébergement static Azure (microsoft).

## Front End :

Tout le site a été réformé pour cette nouvelle version, et le travail effectué a payé : le score d’audit LightHouse :
SEO : 100/100
Accessibilité : 100/100,
Best practices : 100/100,
Le tout grâce à Gatsby et Bulma (css)
