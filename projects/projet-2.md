---
path: /projects/portfolio-2019
date: Avril 2019
title: Mon Portfolio
contentType: projet
lien: ""
tags:
  - Gatsby
  - GraphQL
  - NetlifyCMS
projectImage: /assets/portfolio.jpg
order: 60
---J'ai re-modelé mon portfolio en Mars 2019, tout d'abord en réalisant des wireframes de chaque page puis un design avec le logiciel Adobe XD.

Pour le développement, j'ai choisi d'utiliser le générateur de site statique Gatsby.js couplé au CMS Netlify pour gérer le contenu du site simplement, et avoir un affichage de pages très rapide. <br/>

Depuis le back-office, je peux donc ajouter des nouveaux projets à mon portfolio en quelques clics. <br/>

J'ai conçu l'intégration HTML du site avec l'aide du framework css Bulma, ce qui me permet de gérer l'affichage du site sur mobile et tablette.<br/><br/>

Cet "exercice" me permet aujourdh'ui de vous proposer la création de site avec une gestion administrateur pour gérer du contenu (articles, produits etc.) de façon simple et rapide.
