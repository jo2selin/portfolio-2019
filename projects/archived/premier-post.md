---
path: /projects/opinel
date: Mars 2019
title: Opinel
contentType: projet
lien: 'https://kind-bose-f8ae0c.netlify.com/'
tags:
  - gatsby
  - react
  - GraphQL
projectImage: /assets/opinel.jpg
---
J'ai réalisé ce projet en Février 2019. 

Du design au développement j'ai conçu ce projet de A à Z avec les technologies React, GraphQl et Gatsby.js.

Ce site permet de gérer un catalogue de produits, le tout trié par des tags et catégories. 

Gatsby permet au site de s'afficher beaucoup plus vite qu'un site wordpress, shopify etc. Toutes les pages sont pré-générées en amont, toutes les images sont optimisées à chaque taille d'écran ce qui permet un affichage très rapide.

Pour en savoir plus sur cette technologie : [Your Website Should Be Built With Gatsby](https://www.gatsbyjs.org/blog/2019-04-19-your-website-should-be-built-with-gatsby/)

> "Les sites qui mettent plus de 3s a se charger perdent 53% de leurs visiteurs"
>
> "Dans le domaine de l'e-commerce, on estime à 1% de perte de revenue à chaque délais de chargement de 100ms"
