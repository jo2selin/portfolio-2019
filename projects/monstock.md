---
path: /projects/monstock
date: Juin 2020
title: Monstock.net
contentType: projet
lien: "https://bit.ly/2Xmj8MW"
tags:
  - Gatsby
  - CMS Prismic
  - multilangue
  - SEO
projectImage: /assets/monstock.jpg
order: 90
---

Pour ce projet au budget serré, j'ai réalisé un site multilingue avec l'outil Gatsby-js, éditable via le CMS Prismic.  


J'ai créé tous les champs dans le CMS pour que mon client puisse modifier le contenu de son site, ajouter des pages, des articles de blog, des médias etc.  

---

Afin d'économiser sur le budget, j'ai proposé d'utiliser le CMS Prismic qui propose un compte utilisateur gratuit pour 1 éditeur, ainsi qu'un hébergement chez Netlify qui propose l'hébergement gratuit jusqu'à un certain nombre de visites (100GB bande passante).