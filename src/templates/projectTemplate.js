import React from "react"
import Layout from "../components/layout"
import SEO from "../components/seo"
import Availability from "../components/availability"

import Img from "gatsby-image"
import { graphql, Link } from "gatsby"

const projectTemplate = ({ data }) => {
  // console.log(data)
  const markdownRemark = data.projectData // data.markdownRemark holds our post data
  const { frontmatter, html } = markdownRemark
  const { availability } = data.jsonDataProject.edges[0].node
  const allProjectsImages = data.getAllImages.assetsImages
  // console.log(allProjectsImages)

  const imageProjectPath = frontmatter.projectImage.split("/").slice(2)[0]

  function addImageToProduct(image) {
    const imagePath = image.node.relativePath.split("/").slice(0)[0]
    if (imageProjectPath === imagePath) {
      frontmatter.image = image.node
    }
  }

  allProjectsImages.filter(addImageToProduct)
  // console.log(frontmatter.image)

  return (
    <Layout color="green">
      <SEO
        title={`Projet ${frontmatter.title}`}
        description={`This page describes my work as a freelance, working with ${frontmatter.title}`}
      />
      <section className="section sectionPage">
        <div className="container">
          <div className="columns is-marginless">
            <header className="column headerSplash headerProjectpage is-one-third-tablet header columnContent">
              <h2 className="title titleFatTypo is-2 is-modak">
                {frontmatter.title}
              </h2>
              <h2
                className={`title titleFatTypo is-absolute is-2 is-modak is-green`}
              >
                {frontmatter.title}
              </h2>
              <div className="tags">
                {frontmatter.tags.map((tag, i) => {
                  return (
                    <span key={i} className="tag">
                      {tag}
                    </span>
                  )
                })}
              </div>
              {frontmatter.image && (
                <Img
                  sizes={frontmatter.image.childImageSharp.fluid}
                  className="projectImage is-hidden-tablet"
                />
              )}
              <Link to="/">
                <button className={`button linkToHome is-medium is-green`}>
                  Retour à l'accueil
                </button>
              </Link>
            </header>
            <div className="column">
              <div className="columnContent">
                {frontmatter.image && (
                  <Img
                    sizes={frontmatter.image.childImageSharp.fluid}
                    className="projectImage is-hidden-mobile"
                  />
                )}
                <hr className="is hidden-mobile" />

                <h4 className="title is-4 sectionTitle">Le projet</h4>
                <p className={`is-green`}>
                  <article
                    className="projectDescription"
                    dangerouslySetInnerHTML={{ __html: html }}
                  />
                </p>
                {frontmatter.lien && (
                  <a
                    href={frontmatter.lien}
                    rel="noopener noreferrer"
                    target="_blank"
                  >
                    <button
                      className={`button is-green`}
                      style={{ margin: "20px 0" }}
                    >
                      Voir le projet en ligne
                    </button>
                  </a>
                )}
              </div>
            </div>
            <div className="column">
              <Availability props={availability} />
            </div>
          </div>
        </div>
      </section>
    </Layout>
  )
}

export default projectTemplate

export const pageQuery = graphql`
  query($path: String!) {
    projectData: markdownRemark(frontmatter: { path: { eq: $path } }) {
      html
      frontmatter {
        date
        path
        title
        lien
        tags
        projectImage
      }
    }

    jsonDataProject: allPortfolioJson {
      edges {
        node {
          availability {
            available
            availability_title
            availability_text
          }
        }
      }
    }

    getAllImages: allFile(
      filter: {
        extension: { regex: "/(jpg)/" }
        sourceInstanceName: { eq: "assets" }
      }
    ) {
      assetsImages: edges {
        node {
          id
          relativePath
          childImageSharp {
            fluid {
              ...GatsbyImageSharpFluid
            }
          }
        }
      }
    }
  }
`
