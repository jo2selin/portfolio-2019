import React from "react"
import { graphql, Link } from "gatsby"
import Img from "gatsby-image"

import Layout from "../components/layout"
import SEO from "../components/seo"
import Availability from "../components/availability"

const BioPage = ({ data }) => {
  const { availability, biopage } = data.jsonDataBio.edges[0].node

  return (
    <Layout color={biopage.color}>
      <SEO title="À Propos" description="A propos" />
      <section className="section sectionPage">
        <div className="container">
          <div className="columns is-marginless">
            <header className="column headerSplash headerBiopage is-one-third-tablet header columnContent">
              <h2 className="title titleFatTypo is-2 is-modak">
                {biopage.header.title}
              </h2>
              <h2
                className={`title titleFatTypo is-absolute is-2 is-modak is-${biopage.color}`}
              >
                {biopage.header.title}
              </h2>
              <h1
                className="title titleAboutMe is-1"
                dangerouslySetInnerHTML={{ __html: biopage.header.subtitle }}
              />

              <Img
                fixed={data.perfilePic.childImageSharp.fixed}
                className="profilePic"
                style={{ display: "block" }}
              />

              <Link to="/">
                <button className={`button linkToHome is-medium is-blue`}>
                  Retour à l'accueil
                </button>
              </Link>
            </header>
            <div className="column">
              <div className="columnContent">
                {" "}
                <h4 className="title is-4 sectionTitle">
                  {biopage.body.title}
                </h4>
                <p
                  className={`is-${biopage.color}`}
                  dangerouslySetInnerHTML={{ __html: biopage.body.text }}
                />
                <hr />
                <h4 className="title is-4 sectionTitle">
                  {biopage.business.title}
                </h4>
                <p className={`is-${biopage.color}`}>{biopage.business.text}</p>
                <p className={`is-${biopage.color}`}>
                  {biopage.business.siret}
                </p>
                <p className={`is-${biopage.color}`}>{biopage.business.name}</p>
                <a href="../assets/cgv.pdf" target="_blank">
                  <p
                    className="is-grey"
                    dangerouslySetInnerHTML={{ __html: biopage.cgv.title }}
                  />
                </a>
              </div>
            </div>
            <div className="column">
              <Availability props={availability} className="" />
            </div>
          </div>
        </div>
      </section>
    </Layout>
  )
}

export default BioPage

export const allBioPageQuery = graphql`
  query bioPageQuery {
    jsonDataBio: allPortfolioJson {
      edges {
        node {
          availability {
            available
            availability_title
            availability_text
          }
          biopage {
            color
            header {
              title
              subtitle
            }
            body {
              title
              text
            }
            business {
              title
              text
              siret
              name
            }
            cgv {
              title
              link
            }
          }
        }
      }
    }
    perfilePic: file(relativePath: { eq: "josselin.jpg" }) {
      childImageSharp {
        fixed(width: 200, height: 200) {
          ...GatsbyImageSharpFixed
        }
      }
    }
  }
`
