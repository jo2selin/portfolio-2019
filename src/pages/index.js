import React from "react"
import { graphql, Link } from "gatsby"
import Img from "gatsby-image"

import Layout from "../components/layout"
import SEO from "../components/seo"
import ProjectsList from "../components/projectsList"
import Availability from "../components/availability"

const IndexPage = ({ data }) => {
  const projects = data.allProjectsQuery.edges
  const { availability, homepage } = data.jsonDataIndex.edges[0].node
  const allProjectsImages = data.getAllImages.assetsImages

  return (
    <Layout color={homepage.color}>
      <SEO title="Accueil" description="Page d'accueil du Portfolio" />

      <section className="section sectionPage">
        <div className="container">
          <div className="columns is-marginless">
            <header className="column headerSplash headerHomepage is-one-third-tablet header columnContent">
              <h2 className="title titleFatTypo is-2 is-modak">
                {homepage.header.title}
              </h2>
              <h2 className="title titleFatTypo is-absolute is-2 is-modak is-pink">
                {homepage.header.title}
              </h2>
              <Link to="/bio">
                <h1 className="title titleAboutMe is-1">
                  {`${homepage.header.subtitle} `}
                  <small
                    dangerouslySetInnerHTML={{ __html: homepage.header.text }}
                  />
                </h1>
                <span className="linkToBio">{homepage.header.textButton}</span>
              </Link>

              <Img
                fixed={data.perfilePic.childImageSharp.fixed}
                className="profilePic"
                style={{ display: "block" }}
              />
            </header>

            <div className="column">
              <div className="presentation columnContent">
                <h4 className="title is-4 sectionTitle">
                  {homepage.myWork.title}
                </h4>
                <Link to="/bio">
                  <div
                    className={`is-${homepage.color}`}
                    dangerouslySetInnerHTML={{ __html: homepage.myWork.text1 }}
                  />
                </Link>
                <ul>
                  {homepage.myWork.knowledge.map((know, i) => {
                    return (
                      <li key={i}>
                        <span className="emoji-list">{know.emoji}</span>
                        <p dangerouslySetInnerHTML={{ __html: know.descr }} />
                      </li>
                    )
                  })}
                </ul>
                <section className="section">
                  <h4 className="title is-4 sectionTitle">Ma Spécialité :</h4>
                  <a href="https://gatsby.josselin.pro">
                    <div
                      className={`is-${homepage.color}`}
                      dangerouslySetInnerHTML={{
                        __html: homepage.myWork.text2,
                      }}
                    />
                    <div
                      className={`button is-pink`}
                      style={{
                        marginTop: "20px",
                        padding: "20px",
                        height: "auto",
                        width: "100%",
                        fontSize: "1.3rem",
                      }}
                    >
                      Découvrez mon avis sur Gatsby.js
                    </div>
                  </a>
                </section>
              </div>

              <hr className="is-hidden-tablet" />
              <Availability props={availability} className="is-hidden-tablet" />

              <div className="production columnContent">
                <hr />
                <h4 className="title is-4 sectionTitle">
                  {homepage.production.title_projects}
                </h4>
                {projects.map((project, index) => {
                  return project.node.frontmatter.contentType !==
                    "projet" ? null : (
                    <ProjectsList
                      key={index}
                      props={{ project, allProjectsImages }}
                    />
                  )
                })}
              </div>

              <div className="production columnContent">
                <hr />
                <h4 className="title is-4 sectionTitle">
                  {homepage.production.title_experiences}
                </h4>
                {projects.map((project, index) => {
                  return project.node.frontmatter.contentType !==
                    "experience" ? null : (
                    <ProjectsList
                      key={index}
                      props={{ project, allProjectsImages }}
                    />
                  )
                })}
              </div>
            </div>

            <div className="column is-one-quarter-tablet is-one-third-desktop">
              <Availability
                props={availability}
                className=" is-hidden-mobile"
              />

              <div className="location columnContent">
                <hr />
                <div className="title is-4 sectionTitle">
                  {homepage.location.title}
                </div>

                <div
                  className={`is-${homepage.color}`}
                  dangerouslySetInnerHTML={{ __html: homepage.location.text }}
                />

                <hr />
              </div>
            </div>
          </div>
        </div>
      </section>
    </Layout>
  )
}

export default IndexPage

export const allProjectsQuery = graphql`
  query homePageQuery {
    allProjectsQuery: allMarkdownRemark(
      sort: { fields: frontmatter___order, order: DESC }
    ) {
      edges {
        node {
          html
          frontmatter {
            path
            title
            contentType
            date
            tags
            projectImage
          }
        }
      }
    }

    jsonDataIndex: allPortfolioJson {
      edges {
        node {
          availability {
            available
            availability_title
            availability_text
          }
          homepage {
            color
            header {
              title
              subtitle
              text
              textButton
            }
            myWork {
              title
              text1
              text2
              knowledge {
                descr
                emoji
              }
              tools
            }
            production {
              title_projects
              title_experiences
            }
            location {
              title
              text
            }
          }
        }
      }
    }

    getAllImages: allFile(
      filter: {
        extension: { regex: "/(jpg)/" }
        sourceInstanceName: { eq: "assets" }
      }
    ) {
      assetsImages: edges {
        node {
          id
          relativePath
          childImageSharp {
            fluid {
              ...GatsbyImageSharpFluid
            }
          }
        }
      }
    }

    perfilePic: file(relativePath: { eq: "josselin.jpg" }) {
      childImageSharp {
        fixed(width: 200, height: 200) {
          ...GatsbyImageSharpFixed
        }
      }
    }
  }
`
