import React from "react"
import { graphql, Link } from "gatsby"
import Img from "gatsby-image"

import Layout from "../components/layout"
import SEO from "../components/seo"
import BlogTypeList from "../components/blogTypeList"
import Availability from "../components/availability"

const GatsbyPage = ({ data }) => {
  console.log(data)

  const { availability, gatsbypage } = data.jsonDataGatsby.edges[0].node
  console.log(gatsbypage)

  return (
    <Layout color={gatsbypage.color}>
      <SEO
        title="Gatsby, générateur de site statiques"
        description="Présentation de Gatsby.js , son utilité et ses avantages"
      />
      <section className="section sectionPage blogLayout">
        <div className="container">
          <section className="hero">
            <div className="hero-body">
              <div className="container">
                <h1
                  className="title is-2"
                  dangerouslySetInnerHTML={{ __html: gatsbypage.hero.title }}
                />
                <h2
                  className="subtitle is-4"
                  dangerouslySetInnerHTML={{
                    __html: gatsbypage.hero.subtitle,
                  }}
                />
              </div>
            </div>
          </section>
          <div className="columns is-centered">
            <div className="column is-8">
              <section className="section">
                <div className="container">
                  <div className="columns">
                    <div className="column">
                      <Img
                        fixed={data.perfilePic.childImageSharp.fixed}
                        className="profilePic"
                        style={{ display: "block" }}
                      />
                    </div>
                    <div className="column is-10">
                      <header className="blogHeader">
                        <h2
                          className="title is-4"
                          dangerouslySetInnerHTML={{
                            __html: gatsbypage.header.title,
                          }}
                        />
                      </header>
                    </div>
                  </div>
                </div>
              </section>
              <section className="section is-medium">
                <div className="container">
                  <h3 className="title blogTitle is-5">
                    {gatsbypage.section.gatsby.title}
                  </h3>
                  <p>
                    <span
                      className="blogText"
                      dangerouslySetInnerHTML={{
                        __html: gatsbypage.section.gatsby.intro,
                      }}
                    />
                  </p>
                </div>
              </section>
              <section className="section">
                <div className="container">
                  {gatsbypage.section.gatsby.list.map((item, i) => {
                    return <BlogTypeList listItem={item} />
                  })}
                </div>
              </section>
            </div>
          </div>

          <section className="section">
            <div className="columns is-centered">
              <Link to="/">
                <button className={`button linkToHome is-medium is-blue`}>
                  Retour à l'accueil
                </button>
              </Link>
            </div>
          </section>
        </div>
      </section>
    </Layout>
  )
}

export default GatsbyPage

export const allGatsbyPageQuery = graphql`
  query gatsbyPageQuery {
    jsonDataGatsby: allPortfolioJson {
      edges {
        node {
          availability {
            available
            availability_title
            availability_text
          }
          gatsbypage {
            color
            header {
              title
            }
            hero {
              subtitle
              title
            }
            section {
              gatsby {
                intro
                list {
                  image
                  important
                  less_important
                  title
                }
                title
              }
            }
          }
        }
      }
    }
    perfilePic: file(relativePath: { eq: "josselin.jpg" }) {
      childImageSharp {
        fixed(width: 200, height: 200) {
          ...GatsbyImageSharpFixed
        }
      }
    }
  }
`
