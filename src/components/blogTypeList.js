import React, { Component } from "react"

class BlogTypeList extends Component {
  constructor(props) {
    super(props)
    this.state = { showListContent: false }
    this.handleClick = this.handleClick.bind(this)
  }

  handleClick() {
    this.setState(prevState => ({
      showListContent: !prevState.showListContent,
    }))
  }

  render() {
    console.log("showListContent")
    console.log(this.state.showListContent)
    // the modal you will toggle on and off
    const listContent = (
      <>
        <br />
        <span
          className="blogText less_important"
          onClick={this.handleClick} //close on click when already open
          dangerouslySetInnerHTML={{
            __html: this.props.listItem.less_important,
          }}
        />
      </>
    )

    // the return() to put your default HTML into the DOM
    return (
      <>
        <div className="columns">
          <div className="column blogTypeList">
            <h4 className="title is-blue blogTitle is-6">
              {this.props.listItem.title}
            </h4>
          </div>
          <div className="column is-9">
            <p>
              <span
                className="blogText"
                dangerouslySetInnerHTML={{
                  __html: this.props.listItem.important,
                }}
              />

              {this.state.showListContent ? (
                listContent
              ) : (
                <p className="is-pink readMore" onClick={this.handleClick}>
                  lire la suite
                </p>
              )}
            </p>
          </div>
        </div>
      </>
    )
  }
}
// export the class so you can call it elsewhere
export default BlogTypeList
