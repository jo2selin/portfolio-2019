import PropTypes from "prop-types"
import React from "react"

const Footer = actualColor => {
  // console.log("propsFromLayout")
  // console.log(actualColor)
  // const { actualColor } = actualColor

  return (
    <div className="columns is-marginless">
      <div className="column is-4  is-offset-4 ">
        <footer className="footer sectionPage columnContent">
          <div className={`title is-4 sectionTitle`}>
            Parlons de votre projet
          </div>
          <a href="mailto:josselin.caer@gmail.com?subject=Contact Portfolio">
            <button
              className={`button is-medium is-fullwidth is-${
                actualColor.actualColor
              }`}
            >
              Contactez moi
            </button>
          </a>
        </footer>
      </div>
    </div>
  )
}
Footer.propTypes = {
  actualColor: PropTypes.string,
}

Footer.defaultProps = {
  actualColor: `pink`,
}

export default Footer
