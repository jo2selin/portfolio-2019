import React from "react"
import { Link } from "gatsby"
import Img from "gatsby-image"


const ProjectsList = data => {
  // console.log("List project")
  // console.log("allImages")
  // console.log(data)
  // console.log(data.props)

  const { allProjectsImages, project } = data.props
  const { frontmatter } = project.node

  const imageProjectPath = frontmatter.projectImage.split("/").slice(2)[0]

  function addImageToProduct(image) {
    const imagePath = image.node.relativePath.split("/").slice(0)[0]
    if (imageProjectPath === imagePath) {
      frontmatter.image = image.node
    }
  }

  allProjectsImages.filter(addImageToProduct)
  if (frontmatter.contentType === 'projet'){
    return (
      <Link to={frontmatter.path} props={frontmatter.image}>
        <div className="projectContainer">
          <div className="projectContainerText">
            <div className="projectTitle title is-3">{frontmatter.title}</div>
            <div className="tags">
              {frontmatter.tags.map((tag, i) => {
                return (
                  <span key={i} className="tag">
                    {tag}
                  </span>
                )
              })}
            </div>
            <div className="projectDate">{frontmatter.date}</div>
          </div>
          {frontmatter.image && (
            <Img
              sizes={frontmatter.image.childImageSharp.fluid}
              className="projectImage"
            />
          )}
        </div>
      </Link>
    )
  } else if(frontmatter.contentType === 'experience') {
    return (
        <div className="projectContainer">
          <div className="projectContainerText">
            <div className="projectTitle title is-3">{frontmatter.title}</div>
            <div className="tags">
              {frontmatter.tags.map((tag, i) => {
                return (
                  <span key={i} className="tag">
                    {tag}
                  </span>
                )
              })}
            </div>
            <div className="projectDate">{frontmatter.date}</div>
          </div>
          {frontmatter.image && (
            <Img
              sizes={frontmatter.image.childImageSharp.fluid}
              className="projectImage"
            />
          )}
        </div>
    )
  }
}

export default ProjectsList
