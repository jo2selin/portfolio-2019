import PropTypes from "prop-types"
import React from "react"

const Availability = ({ className, ...rest }) => {
  const { available, availability_title, availability_text } = rest.props

  return available ? (
    <div className={`availability ${className} columnContent`}>
      <div>
        <h3 className="title is-3">{availability_title}</h3>
        <a href="mailto:josselin.caer@gmail.com?subject=Contact Portfolio - Available">
          <p>{availability_text}</p>
        </a>
      </div>
    </div>
  ) : (
    ""
  )
}

Availability.propTypes = {
  availability: PropTypes.object,
}

export default Availability
